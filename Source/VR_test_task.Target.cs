// Copyright © 2022 Dan Hludentsov. All rights reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class VR_test_taskTarget : TargetRules
{
	public VR_test_taskTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Game;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "VR_test_task" } );
	}
}
