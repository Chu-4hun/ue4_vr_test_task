// Copyright © 2022 Dan Hludentsov. All rights reserved.

using UnrealBuildTool;
using System.Collections.Generic;

public class VR_test_taskEditorTarget : TargetRules
{
	public VR_test_taskEditorTarget(TargetInfo Target) : base(Target)
	{
		Type = TargetType.Editor;
		DefaultBuildSettings = BuildSettingsVersion.V2;

		ExtraModuleNames.AddRange( new string[] { "VR_test_task" } );
	}
}
