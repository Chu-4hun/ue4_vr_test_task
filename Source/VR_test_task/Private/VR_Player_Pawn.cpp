// Copyright © 2022 Dan Hludentsov. All rights reserved.


#include "VR_Player_Pawn.h"
DEFINE_LOG_CATEGORY_STATIC(PlayerCharacterLog, All, All);

// Sets default values
AVR_Player_Pawn::AVR_Player_Pawn()
{
	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	SceneComponent = CreateDefaultSubobject<USceneComponent>("VrRoot");
	SceneComponent->SetupAttachment(GetRootComponent());

	LeftHand = CreateDefaultSubobject<UMotionControllerComponent>("LeftMotionControllerComponent");
	LeftHand->SetupAttachment(SceneComponent);

	RightHand = CreateDefaultSubobject<UMotionControllerComponent>("RightMotionControllerComponent");
	RightHand->SetupAttachment(SceneComponent);

	RightHandMesh = CreateDefaultSubobject<USkeletalMeshComponent>("RightHandMesh");
	RightHandMesh->SetupAttachment(RightHand);

	LeftHandMesh = CreateDefaultSubobject<USkeletalMeshComponent>("LeftHandMesh");
	LeftHandMesh->SetupAttachment(LeftHand);

	CameraComponent = CreateDefaultSubobject<UCameraComponent>("CameraComponent");
	CameraComponent->SetupAttachment(SceneComponent);
	CameraComponent->bLockToHmd = true;

	HealthTextComponent = CreateDefaultSubobject<UTextRenderComponent>("HeathWidget");
	HealthTextComponent->SetupAttachment(LeftHand);

	HealthComponent = CreateDefaultSubobject<UHealthComponent>("HealthComponent");
	InteractComponent = CreateDefaultSubobject<UInteractComponent>("InteractComponent");

	
}

// Called when the game starts or when spawned
void AVR_Player_Pawn::BeginPlay()
{
	Super::BeginPlay();
	UHeadMountedDisplayFunctionLibrary::SetTrackingOrigin(EHMDTrackingOrigin::Floor);

	OnHealthChanged(HealthComponent->GetHealth());

	HealthComponent->OnHealthChanged.AddDynamic(this, &AVR_Player_Pawn::OnHealthChanged);
	// HealthComponent->OnDeath.AddDynamic(this, &AVR_Player_Pawn::OnDeath);
}

/**
 * @brief HealthChanged Event Handler / Обработчик событий на изменение здоровья
 * @param Health 
 */
void AVR_Player_Pawn::OnHealthChanged(float Health)
{
	HealthTextComponent->SetText(FText::FromString(FString::Printf(TEXT("%.0f"), Health)));
}

/**
 * @brief Event on death / Эвент по смерти
 */
void AVR_Player_Pawn::OnDeath()
{
	UE_LOG(PlayerCharacterLog, Display, TEXT("Player %s is dead"), *GetName())

	GetCharacterMovement()->DisableMovement();
	UGameplayStatics::OpenLevelBySoftObjectPtr(GetWorld(), GetWorld());
}


// Called every frame
void AVR_Player_Pawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AVR_Player_Pawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	check(PlayerInputComponent);

	PlayerInputComponent->BindAxis("MovementAxisLeft_X", this, &AVR_Player_Pawn::MoveRight);
	PlayerInputComponent->BindAxis("MovementAxisLeft_Y", this, &AVR_Player_Pawn::MoveForward);
	PlayerInputComponent->BindAxis("MovementAxisRight_X", this, &AVR_Player_Pawn::Rotate);

	PlayerInputComponent->BindAction<FInputHandGrabDelegate>("GrabLeft", IE_Pressed, InteractComponent,
	                                                         &UInteractComponent::TryGrab, LeftHand);
	PlayerInputComponent->BindAction<FInputHandGrabDelegate>("GrabLeft", IE_Released, InteractComponent,
	                                                         &UInteractComponent::TryDrop, LeftHand);
	
	PlayerInputComponent->BindAction<FInputHandGrabDelegate>("GrabRight", IE_Pressed, InteractComponent,
	                                                         &UInteractComponent::TryGrab, RightHand);
	PlayerInputComponent->BindAction<FInputHandGrabDelegate>("GrabRight", IE_Released, InteractComponent,
	                                                         &UInteractComponent::TryDrop, RightHand);
	
	PlayerInputComponent->BindAction<FInputHandGrabDelegate>("TriggerRight", IE_Pressed, InteractComponent,
	                                                         &UInteractComponent::TryToPrimaryInteract, RightHand);
	PlayerInputComponent->BindAction<FInputHandGrabDelegate>("SecondaryActionRight", IE_Pressed, InteractComponent,
	                                                         &UInteractComponent::TryToSecondaryInteract, RightHand);
	
	PlayerInputComponent->BindAction<FInputHandGrabDelegate>("TriggerLeft", IE_Pressed, InteractComponent,
	                                                         &UInteractComponent::TryToPrimaryInteract, LeftHand);
	PlayerInputComponent->BindAction<FInputHandGrabDelegate>("SecondaryActionLeft", IE_Pressed, InteractComponent,
	                                                         &UInteractComponent::TryToSecondaryInteract, LeftHand);
}

bool flag = true;

/**
 * @brief Snap turn / Резкий поворот
 * @param Amount degrees to turn
 */
void AVR_Player_Pawn::Rotate(float Amount)
{
	if (IsGreaterThanDeadzone(Amount))
	{
		if (IsSmoothTurn)
		{
			AddControllerYawInput(Amount * TurnSpeedModifier);
		}
		else if (flag)
		{
			const FRotator Rotation = GetActorRotation();
			AddActorLocalRotation(FRotator(0, SnapTurnDegrees, 0));
			// SetActorRotation(FRotator(Rotation.Pitch, Rotation.Yaw + SnapTurnDegrees, Rotation.Roll));

			UE_LOG(PlayerCharacterLog, Log, TEXT("Rotate! %f %f %f"), Rotation.Pitch, Rotation.Yaw, Rotation.Roll);
			flag = false;
		}
	}
	else
	{
		flag = true;
	}
}

/**
 * @brief Moving forward / Передвижение вперед 
 * @param Amount количество 
 */
void AVR_Player_Pawn::MoveForward(float Amount)
{
	AddMovementInput(CameraComponent->GetForwardVector(), Amount);
}

/**
 * @brief Moving to the right / Передвижение вправо
 * @param Amount 
 */
void AVR_Player_Pawn::MoveRight(float Amount)
{
	AddMovementInput(CameraComponent->GetRightVector(), Amount);
}

/**
 * @brief Checks if input AxisValue > AxisDeadzone / Проверяет, является ли Значение отклонения джойстика > Мертвой зоны
 * @param AxisValue 
 * @return is AxisValue > AxisDeadzone?
 */
bool AVR_Player_Pawn::IsGreaterThanDeadzone(float AxisValue)
{
	return abs(AxisValue) >= AxisDeadzone;
}
