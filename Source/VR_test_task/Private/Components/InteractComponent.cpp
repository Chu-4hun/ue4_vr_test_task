// Copyright © 2022 Dan Hludentsov. All rights reserved.


#include "Components/InteractComponent.h"

#include "Components/GrabComponent.h"

DEFINE_LOG_CATEGORY_STATIC(IneractComponentLog, All, All);

// Sets default values for this component's properties
UInteractComponent::UInteractComponent()
{
	PrimaryComponentTick.bCanEverTick = false;
}

// Called when the game starts
void UInteractComponent::BeginPlay()
{
	Super::BeginPlay();
}

/**
 * @brief Checks for nearest interactable AActor* and tries to grab it / Проверяет наличие AActor с которым можно взаимодействовать и берет его в руку
 * @param Hand 
 */
void UInteractComponent::TryGrab(UMotionControllerComponent* Hand)
{
	bool IsRightHand = GetIsRightHandByObjectRef(Hand);
	TArray<AActor*> ListOfActors = TraceForInteractable(Hand);

	if (!ListOfActors.IsValidIndex(0))return;

	UGrabComponent* GrabComponent = Cast<UGrabComponent>(
		GetNearestToHand(Hand, ListOfActors)->GetComponentByClass(UGrabComponent::StaticClass()));
	AActor* GrabObject = GetNearestToHand(Hand, ListOfActors);
	IsRightHand
		? RightHandGrabComponent = GrabComponent
		: LeftHandGrabComponent = GrabComponent;

	IsRightHand
		? RightHandGrabItem = GrabObject
		: LeftHandGrabItem = GrabObject;

	CheckForRepeatedObject(IsRightHand);
	auto* CurrentComponent = IsRightHand
		                         ? RightHandGrabComponent
		                         : LeftHandGrabComponent;
	CurrentComponent->BeginGrab(Hand);
}

/**
 * @brief Drops prop from hand / Роняет предмет из рук
 * @param Hand 
 */
void UInteractComponent::TryDrop(UMotionControllerComponent* Hand)
{
	bool IsRightHand = GetIsRightHandByObjectRef(Hand);
	CheckForRepeatedObject(IsRightHand);
	UGrabComponent* CurrentComponent = IsRightHand ? RightHandGrabComponent : LeftHandGrabComponent;
	if (CurrentComponent == nullptr)return;
	CurrentComponent->BeginDrop(Hand);
}

/**
 * @brief Checks for Repeated Object (Right and Left hand cant hold same AActor) / Проверяет наличие повторяющегося объекта (правая и левая рука не могут удерживать одного и того же AActor)
 * @param IsRightHand 
 */
void UInteractComponent::CheckForRepeatedObject(bool IsRightHand)
{
	if (IsRightHand)
	{
		if (RightHandGrabItem == LeftHandGrabItem)LeftHandGrabItem = nullptr;
		if (RightHandGrabComponent == LeftHandGrabComponent)LeftHandGrabComponent = nullptr;
	}
	else
	{
		if (LeftHandGrabItem == RightHandGrabItem)RightHandGrabItem = nullptr;
		if (LeftHandGrabComponent == RightHandGrabComponent)RightHandGrabComponent = nullptr;
	}
}

/**
 * @brief Gets IsRightHand by UMotionControllerComponent* reference / Получает значение IsRightHand по ссылке UMotionControllerComponent*
 * @param Hand 
 * @return Is Right Hand?
 */
bool UInteractComponent::GetIsRightHandByObjectRef(UMotionControllerComponent* Hand)
{
	if (Hand->MotionSource == "Left")
	{
		return false;
	}
	if (Hand->MotionSource == "Right")
	{
		return true;
	}
	return false;
}

/**
 * @brief Checks if object is in the hand and can interact, after check calls PrimaryInteract()
 * Проверяет, находится ли объект в руке и может ли он взаимодействовать, после проверки вызывает PrimaryInteract()
 * @param Hand 
 */
void UInteractComponent::TryToPrimaryInteract(UMotionControllerComponent* Hand)
{
	const bool IsRightHand = GetIsRightHandByObjectRef(Hand);
	AActor* CurrentItem = IsRightHand ? RightHandGrabItem : LeftHandGrabItem;
	if (CurrentItem == nullptr) return;
	if (!CurrentItem->GetClass()->ImplementsInterface(UInteract_Interface::StaticClass()))return;
	IInteract_Interface::Execute_PrimaryInteract(CurrentItem);
}


// I know what TryToPrimaryInteract() and TryToSecondaryInteract() are similar but i need CurrentItem at the end of the functions
// And creating new Delegate in "VR_Player_Pawn.h", adding bool IsPrimaryInteract to it just for reducing 6 lines of code is strange (•_•)


/**
 * @brief Checks if object is in the hand and can interact, after check calls SecondaryInteract()
 * Проверяет, находится ли объект в руке и может ли он взаимодействовать, после проверки вызывает SecondaryInteract()
 * @param Hand 
 */
void UInteractComponent::TryToSecondaryInteract(UMotionControllerComponent* Hand)
{
	const bool IsRightHand = GetIsRightHandByObjectRef(Hand);
	AActor* CurrentItem = IsRightHand ? RightHandGrabItem : LeftHandGrabItem;
	if (CurrentItem == nullptr) return;
	if (!CurrentItem->GetClass()->ImplementsInterface(UInteract_Interface::StaticClass()))return;
	IInteract_Interface::Execute_SecondaryInteract(CurrentItem);
}

/**
 * @brief Gets all intractable AActors what is near to Hand / Получает все AActor* с которыми можно взаимодействовать
 * @param Hand 
 * @return All received AActors
 */
TArray<AActor*> UInteractComponent::TraceForInteractable(UMotionControllerComponent* Hand)
{
	// create tarray for hit results
	TArray<FHitResult> OutHits;
	TArray<AActor*> HitActors;

	// start and end locations
	FVector SweepStart = Hand->GetComponentLocation();
	FVector SweepEnd = Hand->GetComponentLocation();

	FCollisionShape MyColSphere = FCollisionShape::MakeSphere(GrabCollisionsRadius);

	// DrawDebugSphere(GetWorld(), SweepStart, MyColSphere.GetSphereRadius(), 10, FColor::Emerald, true, 1.0f);

	bool isHit = GetWorld()->SweepMultiByChannel(OutHits, SweepStart, SweepEnd, FQuat::Identity, INTERACTABLE,
	                                             MyColSphere);

	for (FHitResult& Hit : OutHits)
	{
		HitActors.Add(Hit.Actor.Get());
	}

	return HitActors;
}

/**
 * @brief From all received AActors gets nearest to Hand / Из всех полученных Актеров получает ближайший к руке
 * @param Hand 
 * @param Actors 
 * @return nearest AActor* / ближайший AActor*
 */
AActor* UInteractComponent::GetNearestToHand(UMotionControllerComponent* Hand, TArray<AActor*> Actors)
{
	if (!Actors.IsValidIndex(0))return nullptr;
	float Distanse = 0.0f;
	FVector HandLocation = Hand->GetComponentLocation();
	AActor* NearestActor = Actors[0];
	for (auto Actor : Actors)
	{
		float dist = (Actor->GetActorLocation() - HandLocation).SizeSquared();
		if (dist <= Distanse)
		{
			Distanse = dist;
			NearestActor = Actor;
		}
	}
	return NearestActor;
}
