// Copyright © 2022 Dan Hludentsov. All rights reserved.


#include "Components/HealthComponent.h"


// Sets default values for this component's properties
UHealthComponent::UHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

}


// Called when the game starts
void UHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	SetHealth(MaxHealth);

	AActor* ComponentOwner = GetOwner();
	if (ComponentOwner)
	{
		ComponentOwner->OnTakeAnyDamage.AddDynamic(this, &UHealthComponent::OnTakeAnyDamage);
	}
	
}

/**
 * @brief Обработчик событий OnTakeAnyDamage / OnTakeAnyDamage Event Handler 
 * @param DamagedActor Кому пришел урон / Who received the damage
 * @param Damage количество урона / amount of damage
 * @param DamageType тип урона / damage type
 * @param InstigatedBy контроллер кто нанес урон / controller who caused the damage
 * @param DamageCauser кто нанес урон / who caused the damage
 */
void UHealthComponent::OnTakeAnyDamage(AActor* DamagedActor, float Damage, const UDamageType* DamageType,
                                           AController* InstigatedBy, AActor* DamageCauser)
{
	if (Damage <= 0.0f || IsDead() ) return;
	SetHealth(Health - Damage);

	GetWorld()->GetTimerManager().ClearTimer(HealTimerHandle);

	if (IsDead())
	{
		OnDeath.Broadcast();
	}
	else if (AutoHeal.bAutoHealIsEnable)
	{
		GetWorld()->GetTimerManager().SetTimer(HealTimerHandle, this, &UHealthComponent::HealUpdate,
		                                       AutoHeal.HealUpdateTime, true, AutoHeal.TimeToStartAutoHeal);
	}
	// UE_LOG(HealthComponentLog, Display, TEXT("Damage: %f Damage causer: %s"), Damage, *DamageCauser->GetName());
}

/**
 * @brief Обработчик обновления здоровья /
 * Health Update Handler
 */
void UHealthComponent::HealUpdate()
{
	SetHealth(Health + AutoHeal.HealModifier);

	if (FMath::IsNearlyEqual(Health, MaxHealth) && GetWorld())
	{
		GetWorld()->GetTimerManager().ClearTimer(HealTimerHandle);
	}
}
/**
 * @brief Обновление параметра жизни /
 * Updates the life parameter
 * @param NewHealth Новое здоровье / New Health
 */
void UHealthComponent::SetHealth(float NewHealth)
{
	Health = FMath::Clamp(NewHealth, 0.0f, MaxHealth);
	OnHealthChanged.Broadcast(Health);
}
