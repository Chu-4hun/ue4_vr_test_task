// Copyright © 2022 Dan Hludentsov. All rights reserved.


#include "Components/GrabComponent.h"

#include "DrawDebugHelpers.h"
#include "Components/StaticMeshComponent.h"

DEFINE_LOG_CATEGORY_STATIC(GrabComponentLog, All, All);

// Sets default values for this component's properties
UGrabComponent::UGrabComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UGrabComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
}

/**
 * @brief Attach Actor to hand / Приклепляет AActor к руке 
 * @param Hand 
 */
void UGrabComponent::BeginGrab(UMotionControllerComponent* Hand)
{
	//Attach to hand
	Cast<UPrimitiveComponent>(GetAttachParent())->SetSimulatePhysics(false);

	GetAttachParent()->AttachToComponent(Hand,
	                              FAttachmentTransformRules::SnapToTargetNotIncludingScale);
	GetOwner()->SetActorLocation(Hand->GetComponentLocation());
	GetOwner()->SetActorRotation(Hand->GetComponentRotation());
}

/**
 * @brief Detach Actor from hand / Открепляет AActor к руки
 * @param Hand 
 */
void UGrabComponent::BeginDrop(UMotionControllerComponent* Hand)
{
	UE_LOG(GrabComponentLog, Log, TEXT("Detach!"))
	GetAttachParent()->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
	Cast<UPrimitiveComponent>(GetAttachParent())->SetSimulatePhysics(true);
}
