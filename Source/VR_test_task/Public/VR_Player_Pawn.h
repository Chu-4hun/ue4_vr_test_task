// Copyright © 2022 Dan Hludentsov. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Camera/CameraComponent.h"
#include "MotionControllerComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Components/InputComponent.h"
#include "Components/WidgetComponent.h"
#include "UMG.h"
#include "Components/HealthComponent.h"
#include "Components/SkeletalMeshComponent.h"
#include "Components/InteractComponent.h"
#include "GameFramework/Controller.h"
#include "VR_Player_Pawn.generated.h"

DECLARE_DELEGATE_OneParam(FInputHandGrabDelegate, UMotionControllerComponent*);
UCLASS()
class VR_TEST_TASK_API AVR_Player_Pawn : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AVR_Player_Pawn();

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category ="Components")
	UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category ="Components")
	UMotionControllerComponent* LeftHand;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category ="Components")
	UMotionControllerComponent* RightHand;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category ="Components")
	UHealthComponent* HealthComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category ="Components")
	UTextRenderComponent* HealthTextComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category ="Components")
	UInteractComponent* InteractComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category ="Components")
	USkeletalMeshComponent* RightHandMesh;

		UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category ="Components")
	USkeletalMeshComponent* LeftHandMesh;

	

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category ="Input", meta = (ClampMin = "0.0", ClampMax = "1.0"))
	float AxisDeadzone = 0.7;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Input")
	float TurnSpeedModifier = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Input")
	float SnapTurnDegrees = 45.0;
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Input")
	bool IsSmoothTurn = true;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category ="Components")
	USceneComponent* SceneComponent;


protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UFUNCTION()
	void OnHealthChanged(float Health);
	UFUNCTION()
	void OnDeath();




public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;


	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	UFUNCTION()
	void Rotate(float Amount);
	
	UFUNCTION()
	void MoveForward(float Amount);
	
	UFUNCTION()
	void MoveRight(float Amount);
private:
	UFUNCTION()
	bool IsGreaterThanDeadzone(float AxisValue);
	

	
};
