// Copyright © 2022 Dan Hludentsov. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "MotionControllerComponent.h"
#include "Components/ActorComponent.h"
#include "DrawDebugHelpers.h"
#include "GrabComponent.h"
#include "Interfaces/Interact_Interface.h"
#include "InteractComponent.generated.h"

//ECC_GameTraceChannel2 can be found in Config/DefaultEngine.ini in 328 line
#define INTERACTABLE ECC_GameTraceChannel2

UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class VR_TEST_TASK_API UInteractComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UInteractComponent();
	
	UFUNCTION()
	void TryGrab(UMotionControllerComponent* Hand);

	UFUNCTION()
	void TryDrop(UMotionControllerComponent* Hand);
	
	UFUNCTION()
	void TryToPrimaryInteract(UMotionControllerComponent* Hand);
	
	UFUNCTION()
	void TryToSecondaryInteract(UMotionControllerComponent* Hand);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category ="Input")
	float GrabCollisionsRadius = 10.0f;


protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UFUNCTION()
	void CheckForRepeatedObject(bool IsRightHand);


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category ="Components")
	AActor* RightHandGrabItem;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category ="Components")
	AActor* LeftHandGrabItem;

private:
	UPROPERTY(VisibleAnywhere, Category ="Components")
	UGrabComponent* RightHandGrabComponent;

	UPROPERTY(VisibleAnywhere, Category ="Components")
	UGrabComponent* LeftHandGrabComponent;
	
	static bool GetIsRightHandByObjectRef(UMotionControllerComponent* Hand);
	TArray<AActor*> TraceForInteractable(UMotionControllerComponent* Hand);
	AActor* GetNearestToHand(UMotionControllerComponent* Hand, TArray<AActor*> Actors);
};
