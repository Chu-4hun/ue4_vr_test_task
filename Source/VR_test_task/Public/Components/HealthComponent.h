// Copyright © 2022 Dan Hludentsov. All rights reserved.

//I got this from my past project https://gitlab.com/Chu-4hun/ue4_chu_tales/-/blob/668afdbe96da9cde0ce1a0d2f668dfe7833049e2/Source/chu_tales_game/Public/Components/ChT_HealthComponent.h

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "TimerManager.h"
#include "Engine/World.h"
#include "HealthComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDeathSignsture);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnHealthChangedSugnature, float, Health);

USTRUCT(BlueprintType)
struct FAutoHeal
{
	GENERATED_BODY();
	
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal")
	bool bAutoHealIsEnable = false;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta =(EditCondition = "bAutoHealIsEnable"))
	float TimeToStartAutoHeal = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta =(EditCondition = "bAutoHealIsEnable"))
	float HealUpdateTime = 5.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Heal", meta =(EditCondition = "bAutoHealIsEnable"))
	float HealModifier = 1.0f;

};


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class VR_TEST_TASK_API UHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UHealthComponent();

	float GetHealth() const { return Health; }

	UFUNCTION(BlueprintCallable)
	bool IsDead() const {return FMath::IsNearlyZero(Health);};

	UPROPERTY(BlueprintAssignable)
	FOnDeathSignsture OnDeath;
	
	UPROPERTY(BlueprintAssignable)
	FOnHealthChangedSugnature OnHealthChanged;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (ClampMin = 0.0f ))
	float MaxHealth = 100.0f;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Health", meta = (ClampMin = 0.0f ))
	FAutoHeal AutoHeal;
	

public:	
	float Health = 0.0f;
	FTimerHandle HealTimerHandle;

	UFUNCTION()
	void OnTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType,
	                           class AController* InstigatedBy, AActor* DamageCauser);
	
private:
	
	UFUNCTION()
	void HealUpdate();
	UFUNCTION()
	void SetHealth(float NewHealth);
		
};
