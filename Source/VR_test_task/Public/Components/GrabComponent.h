// Copyright © 2022 Dan Hludentsov. All rights reserved.

#pragma once

#include "CoreMinimal.h"
#include "MotionControllerComponent.h"
#include "Components/ActorComponent.h"
#include "GrabComponent.generated.h"


UCLASS(ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class VR_TEST_TASK_API UGrabComponent : public USceneComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UGrabComponent();

	UFUNCTION()
	void BeginGrab(UMotionControllerComponent* Hand);
	
	UFUNCTION()
	void BeginDrop(UMotionControllerComponent* Hand);


protected:
	// Called when the game starts
	virtual void BeginPlay() override;


private:
};
