// Copyright © 2022 Dan Hludentsov. All rights reserved.

#include "VR_test_task.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, VR_test_task, "VR_test_task" );
